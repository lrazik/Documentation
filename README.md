# VILLASframework

<img src="images/logos/villas_framework.svg" align="right" width="150px" >

@subpage framework-architecture

@htmlonly <div style="text-align: center; margin: 2em"><span style="font-size: 1.2em; font-weight: bold; padding: 1em; border: 5px solid #db3e34; border-radius: 20px;"> @endhtmlonly
For starters we recommand to read the @ref node-guide!
@htmlonly </span></div> @endhtmlonly

# Components

- @subpage node
- @subpage web
- @subpage controller
- @subpage fpga

# Download

The source code of all open source components is available over the [RWTH GitLab](https://git.rwth-aachen.de).

**Note:** You need a [Github](https://www.github.com) account to access the repositories.

- @subpage liveusb

# Copyright

2017, Institute for Automation of Complex Power Systems, EONERC

# Licensing

<img alt="GPLv3 logo" src="images/logos/gplv3.png" width="120" align="right"> 

__VILLASnode__, __VILLASweb__ and __VILLAScontroller__ are released as open source software under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).

__VILLASfpga__ is currently closed-source due to propietary firmware components which we can not share.
Please contact [Steffen Vogel](mailto:stvogel@eoenrc.rwth-aachen.de) for details.

Other licensing options available upon request.
Please contact [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de) for further details.

# Contact

<img alt="EONERC ACS Logo" src="images/logos/eonerc_logo.png" align="right" />

- Steffen Vogel <stvogel@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de) 