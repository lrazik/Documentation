# NI LabView {#node-client-labview}

**Source code:** <http://git.rwth-aachen.de/VILLASframework/VILLASnode/tree/develop/clients/labview>

We build an example LabView model which is using the UDP message format of the @ref node-type-socket node-type to communicate with VILLASnode.

@todo Add screenshot

## Contact

Author: Eyke Liegmann <ELiegmann@eonerc.rwth-aachen.de>