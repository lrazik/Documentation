# Nodes {#node-concept-node}

All communication partners are represented by nodes.

Possible types of nodes are:
  * Simulators (OPAL, RTDS)
  * Servers (VILLASnode instances)
  * Workstations (Simulink, Labview, ...)
  * Data Loggers
  * etc..

@see node for implementation details.
