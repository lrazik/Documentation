# Node-types {#node-types}

Every server needs clients which act as sinks / sources for simulation data. In case of VILLASnode these clients are called _nodes_.
Every node is an instance of a node-type. VILLASnode currently supports the following node-types:

- @subpage node-type-fpga
- @subpage node-type-opal
- @subpage node-type-file
- @subpage node-type-socket
- @subpage node-type-websocket
- @subpage node-type-ngsi
- @subpage node-type-cbuilder
- @subpage node-type-shmem
