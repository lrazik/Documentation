# Lab 13: Measure Round-trip time between RTDS and VILLASnode {#node-guide-lab13}

# Configuration

@includelineno lab13.conf

# RSCAD

**Source:** <https://git.rwth-aachen.de/VILLASframework/VILLASnode/tree/develop/clients/rtds/gtnet_skt/gtnet_skt_udp_loopback_rtt>

@image html rscad_gtnet_skt_udp_loopback_rtt_draft.png RSCAD draft for GTNET interface to VILLASnode. width=100%

@image html rscad_gtnet_skt_udp_loopback_rtt_runtime.png RSCAD runtime of GTNET interface to VILLASnode. width=100%