# Requirements {#web-requirements}

## Services
 - NodeJS: Runs web backend
 - MongoDB: Backend database
 - NGinX: Webserver and reverse proxy for backends (only for production)
 - Docker: Container management system

## Installed on your local computer
 - NodeJS with npm
 - Docker
