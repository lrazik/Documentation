# Screenshots {#web-screenshots}

@image html VILLASweb_login.png Login Page width=75%

@image html VILLASweb_notification.png Projects overview with notification width=75%

@image html VILLASweb_plot_widget_edit.png Plot Widget Settings width=75%

@image html VILLASweb_visualization.png Visualization width=75%

# Classic {#web-screenshots-classic}

@image html VILLASweb_classic.png Old interface width=100%